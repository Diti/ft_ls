# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: dtortera <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/02/20 13:26:11 by dtortera          #+#    #+#              #
#    Updated: 2015/04/03 15:03:59 by dtortera         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = ft_ls

FILES = error ft_ls filelist main stat
SRCS = $(addsuffix .c, $(FILES))
OBJS = $(addsuffix .o, $(FILES))

# Compiler & linker flags
CFLAGS = -Werror
ALLCFLAGS = -Wall -Wextra $(CFLAGS)
CPPFLAGS = -iquote inc -iquote libft/inc
LDFLAGS = -L libft -lft

all: libft/libft.a $(NAME)

libft/libft.a:
	@$(MAKE) -C libft

$(NAME): $(OBJS)
	$(CC) $(ALLCFLAGS) $(CPPFLAGS) $(LDFLAGS) $^ -o $@ $(LDFLAGS)

clean:
	$(RM) $(OBJS)

fclean: clean
	$(RM) $(NAME)

re: fclean all

%.o: %.c libft/libft.a
	$(CC) $(ALLCFLAGS) $(CPPFLAGS) -o $@ -c $<

.PHONY: all clean fclean re
