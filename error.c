/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtortera <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/23 15:12:58 by dtortera          #+#    #+#             */
/*   Updated: 2015/04/02 16:17:27 by dtortera         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

/*
** Design:
** GENERIC FUNCTION for getting directory entries
** FUNCTION POINTERS for each option, applied to all entries(?)
** gnu.org/software/libc/manual/html_node/Scanning-Directory-Content.html
** This page is a perfect example of what I have in mind.
*/

int	ft_ls_error(const char *prefix, int errnum)
{
	ft_putstr_fd("ft_ls: ", STDERR_FILENO);
	if (prefix && *prefix != '\0')
	{
		ft_putstr_fd(prefix, STDERR_FILENO);
		ft_putstr_fd(": ", STDERR_FILENO);
	}
	ft_putendl_fd(strerror(errnum), STDERR_FILENO);
	return (errnum);
}
