/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   filelist.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtortera <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/03 15:06:09 by dtortera          #+#    #+#             */
/*   Updated: 2015/04/03 17:21:32 by dtortera         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filelist.h"
#include "libft.h"

t_filelist	*new_filelist(t_fileinfo *file)
{
	t_filelist	*new_lst;

	new_lst = ft_memalloc(sizeof(t_fileinfo));
	if (new_lst)
	{
		new_lst->file = file;
		new_lst->prev = (t_filelist *)NULL;
		new_lst->next = (t_filelist *)NULL;
	}
	return (new_lst);
}

void	add_next_filelist(t_filelist **alst, t_fileinfo *file)
{
	t_filelist	*tmp;
	t_filelist	*new;

	if (alst)
	{
		new = new_filelist(file);
		if (*alst)
		{
			tmp = *alst;
			while (tmp->next)
				tmp = tmp->next;
			tmp->next = new;
			new->prev = tmp;
		}
		else
			*alst = new;
	}
}
