/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ls.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtortera <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/23 15:15:10 by dtortera          #+#    #+#             */
/*   Updated: 2015/04/09 12:41:41 by dtortera         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filelist.h"
#include "ft_ls.h"
#include "stat.h"
#include <errno.h>

#include <dirent.h>

int		dir2list(t_filelist *flst, t_fileinfo *finfo)
{
	// ?
	(void)flst;
	(void)finfo;
	return (-1);
}

int		process_file(t_fileinfo *fileinfo, char *cur_file)
{
	struct stat	buf;
	t_filelist	*flst;

	flst = NULL;
	fileinfo->name = NULL;
	fileinfo->linkname = NULL;
	if (-1 == lstat(cur_file, &buf))
		return (ft_ls_error(cur_file, errno));
	fileinfo->sstat = buf;
	fileinfo->filetype = get_file_type(&buf);
	return (0);
}

void	print_long_listing(t_fileinfo fileinfo)
{
	const char	filetype_char[] = FILETYPE_STR;

	ft_putchar(filetype_char[fileinfo.filetype]);
	ft_putstr("  ");
	if (fileinfo.name)
		ft_putstr(fileinfo.name);
	ft_putchar('\n');
}

void	print_one(t_fileinfo fileinfo)
{
	if (fileinfo.name)
		ft_putstr(fileinfo.name);
	ft_putchar('\n');
}

void	print(t_fileinfo fileinfo, unsigned int opts)
{

	if (opts & OPT_LONG_LISTING)
		print_long_listing(fileinfo);
	else
		print_one(fileinfo);
}

void	ft_ls(char **files, unsigned int opts)
{
	char		**cur_file;
	t_fileinfo	fileinfo;

	cur_file = files;
	while ((*cur_file = *files++) != NULL)
	{
		if (0 == process_file(&fileinfo, *cur_file)) // dir2list
			print(fileinfo, opts);
	}
}
