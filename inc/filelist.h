/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   filelist.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtortera <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/03 14:45:33 by dtortera          #+#    #+#             */
/*   Updated: 2015/04/03 15:14:29 by dtortera         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FILELIST_H
# define FILELIST_H

# include "stat.h"

typedef struct s_filelist	t_filelist;

struct						s_filelist
{
	t_fileinfo	*file;
	t_filelist	*prev;
	t_filelist	*next;
};

t_filelist		*new_filelist(t_fileinfo *file);
void			add_filelist(t_filelist **lst, t_filelist *src);
void			sort_by_time(t_filelist **lst);

#endif
