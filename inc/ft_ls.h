/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ls.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtortera <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/23 15:09:27 by dtortera          #+#    #+#             */
/*   Updated: 2015/04/02 16:16:56 by dtortera         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_LS_H
# define FT_LS_H

# include "libft.h"
# include <dirent.h>
# include <errno.h>
# include <grp.h>
# include <pwd.h>
# include <stdio.h>
# include <stdlib.h>
# include <string.h>
# include <sys/types.h>
# include <time.h>
# include <unistd.h>
# include <uuid/uuid.h>

# define OPT_LONG_LISTING		0x01
# define OPT_RECURS_SUBDIRS		0x02
# define OPT_INCLUDE_HIDDEN		0x04
# define OPT_REVERSE_SORT		0x08
# define OPT_SORT_TIME_M		0x10

int		ft_ls_error(const char *prefix, int errnum);

void	ft_ls(char **files, unsigned int opts);

#endif
