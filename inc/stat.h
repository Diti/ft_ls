/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stat.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtortera <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/02 15:31:53 by dtortera          #+#    #+#             */
/*   Updated: 2015/04/09 12:27:07 by dtortera         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef STAT_H
# define STAT_H

# include "libft.h"
# include <sys/stat.h>

# define FILETYPE_STR	("pcdb-lsw?")

typedef enum	e_filetype
{
	fifo,
	chardev,
	directory,
	blockdev,
	normal,
	symbolic_link,
	socket,
	whiteout,
	unknown
}				t_filetype;

typedef struct	s_fileinfo
{
	char			*name;
	char			*linkname;
	struct stat		sstat;
	enum e_filetype	filetype;
}				t_fileinfo;

t_filetype		get_file_type(struct stat *buf);
void			print_file_rights(struct stat *buf);
void			print_uid_gid(struct stat *buf);
void			print_entry_stat(struct stat *buf);

#endif
