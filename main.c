/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtortera <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/20 13:31:35 by dtortera          #+#    #+#             */
/*   Updated: 2015/04/02 16:17:13 by dtortera         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void	ft_get_option(char c, unsigned int *opts)
{
	if (c == 'a')
		*opts |= OPT_INCLUDE_HIDDEN;
	else if (c == 'l')
		*opts |= OPT_LONG_LISTING;
	else
	{
		ft_putstr_fd("Unknown option: -", STDERR_FILENO);
		ft_putchar_fd(c, STDERR_FILENO);
		ft_putchar_fd('\n', STDERR_FILENO);
		exit(1);
	}
}

void	ft_read_option(char *str, unsigned int *opts)
{
	while (*str)
	{
		ft_get_option(*str, opts);
		str++;
	}
}

int		parse_args(char **av, unsigned int *opts)
{
	int		optionsno;

	*opts = 0;
	optionsno = 0;
	while (*av)
	{
		if (**av == '-')
		{
			ft_read_option(*av + 1, opts);
			optionsno++;
		}
		else
			break ;
		av++;
	}
	return (optionsno);
}

int		main(int argc, char *argv[])
{
	char			**files;
	int				optionsno;
	unsigned int	opts;

	argc--;
	argv++;
	files = NULL;
	optionsno = parse_args(argv, &opts);
	if ((argc - optionsno) == 0)
	{
		files = (char **)ft_memalloc(2 * sizeof(char *));
		if (files)
			files[0] = ".";
		else
			exit(ft_ls_error("ft_memalloc", errno));
	}
	else
		files = argv + optionsno;
	ft_ls(files, opts);
	if ((argc - optionsno) == 0)
		ft_memdel((void **)&files);
	return (0);
}
