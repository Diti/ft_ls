/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stat.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtortera <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/26 16:40:44 by dtortera          #+#    #+#             */
/*   Updated: 2015/04/02 16:59:07 by dtortera         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "stat.h"

t_filetype		get_file_type(struct stat *sstat)
{
	enum e_filetype	type;
	const mode_t	mode = sstat->st_mode;

	if ((mode & S_IFMT) == S_IFIFO)
		type = fifo;
	else if ((mode & S_IFMT) == S_IFCHR)
		type = chardev;
	else if ((mode & S_IFMT) == S_IFDIR)
		type = directory;
	else if ((mode & S_IFMT) == S_IFBLK)
		type = blockdev;
	else if ((mode & S_IFMT) == S_IFREG)
		type = normal;
	else if ((mode & S_IFMT) == S_IFLNK)
		type = symbolic_link;
	else if ((mode & S_IFMT) == S_IFSOCK)
		type = socket;
	else if ((mode & S_IFMT) == S_IFWHT)
		type = whiteout;
	else
		type = unknown;
	return (type);
}

/*void	print_file_rights(struct stat *buf)
{
	const mode_t	mode = buf->st_mode;

	ft_putchar((mode & S_IRUSR) ? 'r' : '-');
	ft_putchar((mode & S_IWUSR) ? 'w' : '-');
	if ((mode & S_ISUID))
		ft_putchar((mode & S_IXUSR) ? 's' : 'S');
	else
		ft_putchar((mode & S_IXUSR) ? 'x' : '-');
	ft_putchar((mode & S_IRGRP) ? 'r' : '-');
	ft_putchar((mode & S_IWGRP) ? 'w' : '-');
	if ((mode & S_ISGID))
		ft_putchar((mode & S_IXGRP) ? 's' : 'S');
	else
		ft_putchar((mode & S_IXGRP) ? 'x' : '-');
	ft_putchar((mode & S_IROTH) ? 'r' : '-');
	ft_putchar((mode & S_IWOTH) ? 'w' : '-');
	ft_putchar((mode & S_IXOTH) ? 'x' : '-');
}

void	print_uid_gid(struct stat *buf)
{
	const struct passwd	*pwd = getpwuid(buf->st_uid);
	const struct group	*grp = getgrgid(buf->st_gid);

	if (pwd && pwd->pw_name)
		ft_putstr(pwd->pw_name);
	else
		ft_putnbr(buf->st_uid);
	ft_putstr("  ");
	if (grp && grp->gr_name)
		ft_putstr(grp->gr_name);
	else
		ft_putnbr(buf->st_gid);
}

void	print_entry_stat(struct stat *buf)
{
	const struct stat	finfo = *buf;
	
	ft_putchar(get_file_type(buf));
	print_file_rights(buf);
	ft_putstr("  ");
	ft_putnbr(finfo.st_nlink); // If 2-digits, we must keep the info that we need additional space
	ft_putchar(' ');
	print_uid_gid(buf);
	ft_putstr("  ");
	ft_putnbr(finfo.st_size);
	ft_putchar(' ');
}*/
